const int TRIG = 3;
const int ECHO = 5;
const int GREEN_LED = 8;
const int RED_LED = 10;

long echoPulseLength;
double distance;

void setup() {
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);

  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);

  Serial.begin(9600);                     //setup serial port for monitoring purpose
}

void loop() {  
  digitalWrite(TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG, LOW);
 
  echoPulseLength = pulseIn(ECHO, HIGH);   //measure length of pulse

  distance = echoPulseLength / 58.0;       //distance in cm

  if (distance > 10) {
    digitalWrite(GREEN_LED, HIGH);
    digitalWrite(RED_LED, LOW);
  } else {
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(RED_LED, HIGH);
  }

  Serial.print("Measured distance: ");
  Serial.print(distance);
  Serial.println(" cm");
  delay(250);
}
