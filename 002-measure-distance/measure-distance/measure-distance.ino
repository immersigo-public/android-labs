const int TRIG = 3;
const int ECHO = 5;

long echoPulseLength;
double distance;

void setup() {
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);

  Serial.begin(9600);                     //setup serial port for monitoring purpose
}

void loop() {  
  digitalWrite(TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG, LOW);
 
  echoPulseLength = pulseIn(ECHO, HIGH);   //measure length of pulse

  distance = echoPulseLength / 58.0;       //distance in cm
  Serial.print("Measured distance: ");
  Serial.print(distance);
  Serial.println(" cm");
  delay(250);
}
