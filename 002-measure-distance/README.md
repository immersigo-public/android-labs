# Distance Measurements

In this exercise we will use Arduino to measure distance using ultrasonic ranging module [HC-SR04](https://www.electroschematics.com/wp-content/uploads/2013/07/HCSR04-datasheet-version-1.pdf)


## Equipment
* Arduino UNO
* ultrasonic ranging module HC-SR04
* 2 leds and 2 resistors (330 ohms) for extended version of exercise


## Schemat

<img src="measure_distance.jpg" alt="HC-SR04 read value arduino schemat" width="80%"/>