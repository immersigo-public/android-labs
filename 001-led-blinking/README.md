# LED blinking

In this exercise we will use Arduino to blink LED. Blinking pattern will be programmed in Arduino.


## Equipment
* Arduino UNO
* LED
* 330 ohms resistor

## Schemat

<img src="led_blinking.jpg" alt="led blinking arduino schemat" width="80%"/>


## Bonus

Modify code to blink the led with the pattern provided by instructor.