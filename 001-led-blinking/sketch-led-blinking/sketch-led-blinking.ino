const int ledPin =  8;          // the number of the LED pin

void setup() {
  pinMode(ledPin, OUTPUT);      //how the pin will be used
}

void loop() {
  digitalWrite(ledPin, HIGH);   //send high signal to the pin; turn led on
  delay(1000);

  digitalWrite(ledPin, LOW);    //turn led off
  delay(500);
}
