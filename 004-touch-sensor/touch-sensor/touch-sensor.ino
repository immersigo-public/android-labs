const int TOUCH_PIN = 3;
int touch_value;

void setup() {
  pinMode(TOUCH_PIN, INPUT);

  Serial.begin(9600);
}

void loop() {
  touch_value = digitalRead(TOUCH_PIN);
  Serial.print("Touch: ");
  Serial.println(touch_value);
  delay(50);
}
