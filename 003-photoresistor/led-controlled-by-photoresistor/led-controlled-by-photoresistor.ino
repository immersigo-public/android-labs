const int LED_PIN = 8;
const int FOTORESISTOR = A1;
int value = 0;

void setup() {
  pinMode(LED_PIN, OUTPUT);      //how the pin will be used
  Serial.begin(9600);
}

void loop() {
  value = analogRead(FOTORESISTOR);
  Serial.println(value);

  if (value < 200) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }
  
  delay(200);
}
