# Photoresistor

In this exercise we will use Arduino to measure light intensity using [photoresistor](https://pl.wikipedia.org/wiki/Fotorezystor)

## Equipment
* Arduino UNO
* photoresistor
* 1k ohm resistor

## Schemat

<img src="photoresistor_read_value.jpg" alt="photoresistor read value arduino schemat" width="80%"/>